<?php

namespace App\Listeners;

use App\Events\Event;
use App\Mail\AdminNotify;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUserWelcome;
use App\Admin;

class EventListener
{
    protected $admins;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Admin $admin)
    {
        $this->admins = $admin->getAdminEmail();
    }

    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        $email = $this->admins;

        if ($event->user->email) {
            Mail::to($event->user->email)->send(new NewUserWelcome());
        }

        if ($email) {
            $email->map(function ($item) {
                if ($item->email) {
                    Mail::to($item->email)->send(new AdminNotify());
                }
            });
        }

    }
}
