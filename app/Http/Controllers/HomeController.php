<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $profile = User::find(Auth::user()->id)->profile;
        $users = User::all();

        return view('home', compact(['profile', 'users']));
    }

    public function create()
    {

        return view('create');
    }

    public function store(Request $request)
    {
        $profile = new Profile([
            'user_id' => Auth::user()->id,
            'gender' => $request->get('gender'),
            'name' => $request->get('name'),
            'about' => $request->get('about')
        ]);
        $profile->save();
        return redirect('home');

    }

    public function edit($id)
    {
        $profile = Profile::find($id);
        return view('edit', compact('profile'));

    }

    public function update(Request $request, $id)
    {
        $profile = Profile::find($id);
        $profile->gender = $request->get('gender');
        $profile->name = $request->get('name');
        $profile->about = $request->get('about');
        $profile->user_id = $request->get('user-id');
        $profile->save();

        return redirect('home');

    }

    public function getUser($id)
    {
        $n = 0;

        $profile = User::find($id)->profile;

        $currentProfile = User::find(Auth::user()->id)->profile;

        if ($currentProfile) {

            if ($currentProfile->gender == 0) {
                $n++;
            }
            if ($currentProfile->name == null) {
                $n++;
            }
            if ($currentProfile->about == null) {
                $n++;
            }

        } else {

            $n = 3;
        }


        if ($n == 0) {

            return view('user-profile', compact(['profile', 'n']));

        } else {

            Session::flash("message", "You should typy you profile after you will show another profile");

            return redirect()->back();
        }

    }

}
