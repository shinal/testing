@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">USER Description</div>

                    <div class="panel-body">
                        @if($profile != null)

                            <p><strong>Name: {{ $profile->name }}</strong></p>
                            <p><strong>About: {{ $profile->about }}</strong></p>
                            @if($profile->gender == 0)
                                <p><strong>Gender: Not choose</strong></p>
                            @elseif($profile->gender == 1)
                                <p><strong>Gender: Male</strong></p>
                            @elseif($profile->gender == 2)
                                <p><strong>Gender: Female</strong></p>
                            @endif
                        @else
                            <p>This user not have content about youself</p>
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
