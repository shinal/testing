@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card-body">
            {!! Form::open(array('url' => 'update/' . Auth::user()->profile->id, 'method' => 'PUT')) !!}

            <div class="form-group">
                <label for="gender">Gender</label>
                <select name="gender" class="form-control">
                    <option value="{{ ($profile->gender == 0) ?: '0' }}" {{ ($profile->gender == 0) ? "selected" : "" }}>
                        Choose...
                    </option>
                    <option value="{{ ($profile->gender == 1) ?: '1' }}" {{ ($profile->gender == 1) ? "selected" : "" }}>
                        Male
                    </option>
                    <option value="{{ ($profile->gender == 2) ?: '2' }}" {{ ($profile->gender == 2) ? "selected" : "" }}>
                        Female
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name"
                       value="{{ ($profile->name != null) ? $profile->name : "Enter name" }}">
                <small id="emailHelp" class="form-text text-muted">We'll never share your name with anyone else.
                </small>
            </div>
            <div class="form-group">
                <label for="about">Description</label>
                <input type="text" class="form-control" name="about"
                       value="{{ ($profile->name != null) ? $profile->about : "Enter about" }}">
            </div>


            <div class="input-user-id">
                <input type="hidden" name="user-id" value={{ Auth::user()->id }}>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
