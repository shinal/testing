@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">USER Dashboard</div>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <div class="panel-body">
                        @if($profile != null)
                            <p><strong>Name: {{ $profile->name }}</strong></p>
                            <p><strong>About: {{ $profile->about }}</strong></p>
                            @if($profile->gender == 0)
                                <p><strong>Gender: Not choose</strong></p>
                            @elseif($profile->gender == 1)
                                <p><strong>Gender: Male</strong></p>
                            @elseif($profile->gender == 2)
                                <p><strong>Gender: Female</strong></p>
                            @endif
                            <a href="{{ route('profile.edit', $profile->id) }}">Edit</a>
                        @else
                            <a href="{{ route('profile.create') }}">Create profile</a>
                        @endif
                    </div>

                </div>

            </div>
            <div class="col-md-2 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Anothe users</div>
                    <div class="panel-body">
                        @if($users != null)
                            @foreach($users as $user)
                                <div>
                                    <p>
                                        <a href="{{ url('user/' . $user->id ) }}"><strong>Name: {{ $user->name }}</strong></a>
                                    </p>
                                </div>
                            @endforeach

                        @else
                            <p>Users not found</p>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
