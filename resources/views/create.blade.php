@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card-body">
            {!! Form::open(array('url' => 'store', 'method' => 'POST')) !!}

            <div class="form-group">
                <label for="gender">Gender</label>
                <select name="gender" class="form-control">
                    <option value="0" selected>Choose...</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Enter name">
                <small id="emailHelp" class="form-text text-muted">We'll never share your name with anyone else.
                </small>
            </div>
            <div class="form-group">
                <label for="about">Description</label>
                <input type="text" class="form-control" name="about" placeholder="About you">
            </div>


            <div class="input-user-id">
                <input type="hidden" name="user-id" value={{ Auth::user()->id }}>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
