@component('mail::message')
# notify about new auth user

Notify about new auth user.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
