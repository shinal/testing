<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Testing

1. git clone https://gitlab.com/shinal/testing.git
2. Configure .env file field


For database:

DB_DATABASE=

DB_USERNAME=

DB_PASSWORD=

For email notification through mailtrap

MAIL_DRIVER=smtp

MAIL_HOST=smtp.mailtrap.io

MAIL_PORT=2525

MAIL_USERNAME=

MAIL_PASSWORD=

MAIL_FROM_ADDRESS=

MAIL_FROM_NAME=

3. composer install
4. php artisan migrate
5. php artisan serve

